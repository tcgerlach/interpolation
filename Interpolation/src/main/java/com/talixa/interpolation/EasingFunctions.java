package com.talixa.interpolation;

/**
 * Easing function for interpolation.
 * @author tcgerlach
 */
public final class EasingFunctions {

	/**
	 * Linear easing - does not change u at all.
	 * @param u time value between 0 and 1
	 * @return unmodified u value
	 */
	public static float linear(final float u) {
		return u;
	}
	
	/**
	 * Ease in - slow at the beginning, then speeds up.
	 * @param u time value between 0 and 1
	 * @param mod scale for easing u - higher numbers scale more drastically
	 * @return modified u value
	 */
	public static float easeIn(final float u, final float mod) {
		return pow(u,mod);
	}
	
	/**
	 * Ease out - starts fast, then slows down.
	 * @param u time value between 0 and 1
	 * @param mod scale for easing u - higher numbers scale more drastically
	 * @return modified u value
	 */
	public static float easeOut(final float u, final float mod) {
		return 1 - pow(1-u, mod);
	}
	
	/**
	 * Ease both in and out - slow at beginning and end, fast in the middle.
	 * @param u time value between 0 and 1
	 * @param mod scale for easing u - higher numbers are more drastic
	 * @return modified u value
	 */
	public static float easeInOut(final float u, final float mod) {
		if (u <= 0.5f ) {
            return 0.5f * pow(u*2, mod);
        } else {
            return 0.5f + 0.5f * (1 - pow( 1-(2*(u-0.5f)), mod ));
        }
	}
	
	/**
	 * Sin function - causes retrograde movement in the middle of the cycle
	 * @param u time value from 0 to 1
	 * @param mod -.2 to .2 cause small retrograde movement, larger numbers are more dramatic
	 * @return modified u value
	 */
	public static float sin(final float u, final float mod) {
		return u + mod * sin(u);
	}
	
	/**
	 * Sin in - slow entering, then speeds up. Similar to easeIn but smoother.
	 * @param u time value from 0 to 1
	 * @return modified u value
	 */
	public static float sinIn(final float u) {
		return (float)(1 - Math.cos(u * Math.PI * 0.5f));
	}
	
	/**
	 * Sin out - slow exiting. Similar to easeOut but smoother.
	 * @param u time value from 0 to 1
	 * @return modified u value
	 */
	public static float sinOut(final float u) {
		return (float)Math.sin(u * Math.PI * 0.5f);
	}
	
	private static float pow(final float x, final float exp) {
		return (float)Math.pow(x, exp);
	}
	
	private static float sin(final float x) {
		return (float)Math.sin(2 * Math.PI * x);
	}
	
	private EasingFunctions() {
		// hide constructor - all static methods
	}
}
