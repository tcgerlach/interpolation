package com.talixa.interpolation;

import org.junit.Assert;
import org.junit.Test;

public class InterpolatorTest {
	
	private static final double TOLERANCE = .000001;

	@Test
	public void testOneDimensionInterpolation() {
		for(int i = 0; i < 10; ++i) {
			float value = Interpolator.interpolate(0, 10, ((float)i)/10);
			Assert.assertEquals(i, value, TOLERANCE);
		}
	}
	
	@Test
	public void testTwoDimensionInterpolation() {
		float[] p1 = new float[] {0f, 0f};
		float[] p2 = new float[] {10f, 10f};
		
		for(int i = 0; i < 10; ++i) {
			float[] value = Interpolator.interpolate(p1, p2, ((float)i)/10);
			Assert.assertEquals(i, value[0], TOLERANCE);
			Assert.assertEquals(i, value[1], TOLERANCE);
		}
	}
	
	@Test
	public void test3PointBezierCurve() {
		float[] p1 = new float[] {0f, 0f};
		float[] p2 = new float[] {0f, 3f};
		float[] p3 = new float[] {3f, 3f};
		
		// at time 0, should be at 0,0
		float[] t1 = Interpolator.bezierCurve(p1, p2, p3, 0);
		Assert.assertEquals(0, t1[0], TOLERANCE);
		Assert.assertEquals(0, t1[1], TOLERANCE);
		
		// half way should be at 0.75,2.25
		float[] t2 = Interpolator.bezierCurve(p1, p2, p3, .5f);
		Assert.assertEquals(0.75, t2[0], TOLERANCE);
		Assert.assertEquals(2.25, t2[1], TOLERANCE);
		
		// at end, should be 3,3
		float[] t3 = Interpolator.bezierCurve(p1, p2, p3, 1);
		Assert.assertEquals(3, t3[0], TOLERANCE);
		Assert.assertEquals(3, t3[1], TOLERANCE);
	}
	
	@Test(expected=RuntimeException.class)
	public void testInterpolatorDimensionMismatch() {
		float[] p1 = new float[] {0f,1f};
		float[] p2 = new float[] {0f,1f,2f};
		Interpolator.interpolate(p1, p2, 0);
	}
	
	@Test(expected=RuntimeException.class) 
	public void testBezierCurve3PointDimensionMismatch() {
		float[] p1 = new float[] {0,1};
		float[] p2 = new float[] {0,1,2};
		float[] p3 = new float[] {0,1,2,3};
		Interpolator.bezierCurve(p1, p2, p3, .5f);
	}
	
	@Test(expected=RuntimeException.class) 
	public void testBezierCurve4PointDimensionMismatch() {
		float[] p1 = new float[] {0,1};
		float[] p2 = new float[] {0,1,2};
		float[] p3 = new float[] {0,1,2,3};
		float[] p4 = new float[] {0,1,2,3,4};
		Interpolator.bezierCurve(p1, p2, p3, p4, .5f);
	}
}
