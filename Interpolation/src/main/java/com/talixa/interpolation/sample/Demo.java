package com.talixa.interpolation.sample;

import com.talixa.interpolation.EasingFunctions;
import com.talixa.interpolation.Interpolator;

public class Demo {

	private static final float[] P1 = new float[] {0f, 0f};
	private static final float[] P2 = new float[] {0f, 3f};
	private static final float[] P3 = new float[] {3f, 0f};
	private static final float[] P4 = new float[] {3f, 3f};
	private static final int NUM_DATA_POINTS = 10;

	public static void main(final String[] args) {
		// this will run the bezier curve for the entered points
		// and then output the code to plot in R studio

		StringBuilder x = new StringBuilder("x <- c(");
		StringBuilder y = new StringBuilder("y <- c(");

		for(int i = 0; i <= NUM_DATA_POINTS; ++i) {
			float distance = ((float)i)/NUM_DATA_POINTS;
			float u = EasingFunctions.sinOut(distance);

			float[] r = Interpolator.bezierCurve(P1, P2, P3, P4, u);
			x.append(r[0]);
			y.append(r[1]);

			if (i != 10) {
				x.append(",");
				y.append(",");
			}
		}

		System.out.println(x + ")");
		System.out.println(y + ")");
		System.out.println("plot(x,y)");
	}
}
