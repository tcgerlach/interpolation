package com.talixa.interpolation;

/**
 * Functions for interpolation and bezier curve data point generation.
 * @author tcgerlach
 */
public final class Interpolator {

	/**
	 * Interpolate between two points.
	 * @param p1 first point
	 * @param p2 second point
	 * @param u value from 0 to 1 indicating percentage of distance between points
	 * @return interpolated point
	 */
	public static float interpolate(final float p1, final float p2, final float u) {
		return (p1 * (1-u)) + (p2 * u);
	}

	/**
	 * Interpolate between two points.
	 * @param p1 first n-dimensional point
	 * @param p2 second n-dimensional point
	 * @param u percentage of distance from p1 to p2
	 * @return interpolated point
	 */
	public static float[] interpolate(final float[] p1, final float[] p2, final float u) {
		if (p1.length != p2.length) {
			throw new RuntimeException("Array dimensions do not match");
		}

		// interpolate each index of the input array
		float[] res = new float[p1.length];
		for(int i = 0; i < p1.length; ++i) {
			res[i] = interpolate(p1[i], p2[i], u);
		}

		return res;
	}
	
	/**
	 * Find a point on a bezier curve defined by three n-dimensional control points.
	 * @param p1 first point
	 * @param p2 second point
	 * @param p3 third point
	 * @param u distance on line from 0 to 1
	 * @return interpolated point
	 */
	public static float[] bezierCurve(final float[] p1, final float[] p2, final float[] p3, final float u) {
		if (p1.length != p2.length || p2.length != p3.length) {
			throw new RuntimeException("Array dimensions do not match");
		}

		// calculate intermediate points
		float[] p12 = interpolate(p1,p2,u);
		float[] p23 = interpolate(p2,p3,u);

		// calculate final result
		return interpolate(p12,p23,u);
	}

	/**
	 * Find a point on a bezier curve defined by four n-dimensional control points.
	 * @param p1 first point
	 * @param p2 second point
	 * @param p3 third point
	 * @param p4 fourth point
	 * @param u distance on line from 0 to 1
	 * @return interpolated point
	 */
	public static float[] bezierCurve(final float[] p1, final float[] p2, final float[] p3, final float[] p4, final float u) {
		if (p1.length != p2.length || p2.length != p3.length || p3.length != p4.length) {
			throw new RuntimeException("Array dimensions do not match");
		}

		// calculate intermediate points
		float[] p12 = interpolate(p1,p2,u);
		float[] p23 = interpolate(p2,p3,u);
		float[] p34 = interpolate(p3,p4,u);

		// calculate second order intermediate points
		float[] p1223 = interpolate(p12,p23,u);
		float[] p2334 = interpolate(p23,p34,u);

		// calculate final result
		return interpolate(p1223,p2334,u);
	}

	private Interpolator() {
		// nothing but static functions - no constructor necessary
	}
}
